#pragma once

#include <vector>

/**
 * Implement the following interface.
 * 
 * Store data in memory. (database is not needed)
 * Performance matters.
 * The interface will be called from multiple threads.
 * Do not hesitate to write unit tests.
 * 
 * Your class will be instantiated and called as such:
 * Implementation impl* = new Implementation();
 * impl->registerUser(1);
 * impl->post(1, 5);
 * impl->registerUser(2);
 * impl->post(2, 5);
 * impl->follow(1, 2);
 * impl->getNewsFeed(1);
 * impl->unfollow(1, 2);
 * ...
 */
class Interface
{
public:
    /**
     * Registers a new user.
     */
    virtual void registerUser(int userId) = 0;

    /**
     * User adds a new post.
     */
    virtual void post(int userId, int postId) = 0;

    /**
     * Retrieve the 5 most recent posts in the user's news feed.
     * Each item in the news feed must be posted
     * by users who the user followed or by the user herself.
     * Posts must be ordered from most recent to least recent.
     */
    virtual std::vector<int> getNewsFeed(int userId) = 0;

    /**
     * Retrieve the n most recent posts in the user's news feed.
     * Each item in the news feed must be posted
     * by users who the user followed or by the user herself.
     * Posts must be ordered from most recent to least recent.
     */
    virtual std::vector<int> getNewsFeed(int userId, int n) = 0;

    /**
     * Follower follows a followee.
     */
    virtual void follow(int followerId, int followeeId) = 0;

    /**
     * Follower unfollows a followee.
     */
    virtual void unfollow(int followerId, int followeeId) = 0;
};
