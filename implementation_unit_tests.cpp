#include "implementation.h"

#include <iostream>
#include <cassert>
#include <string>

void getFeedForUserOnlyOneTest()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);
    impl->follow(1, 2);
    impl->post(2, 10);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.at(0) == 10);
}

void getFeedForUserOnlyMultiplePostTest()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);
    impl->follow(1, 2);
    impl->post(2, 10);
    impl->post(2, 11);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.at(0) == 10);
    assert(feed.at(1) == 11);
}

void followThenAddPostsThenUnfollowThenNoNewsOnFeedTest()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);
    impl->follow(1, 2);
    impl->post(2, 10);
    impl->unfollow(1, 2);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.size() == 0);
}

void followAfterPostsButStillSeePosts()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);
    impl->post(2, 10);
    impl->follow(1, 2);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.at(0) == 10);
}

void multiplePostForOneFollowerTest()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);
    impl->registerUser(3);

    impl->follow(1, 2);
    impl->follow(1, 3);

    impl->post(2, 10);
    impl->post(3, 11);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.at(0) == 10);
    assert(feed.at(1) == 11);
}

void willNotSeePostIfNotFollowingUser()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);

    impl->post(2, 10);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.size() == 0);
}

void willGetFeedWithSize1WithMultiplePosts()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);

    impl->follow(1, 2);

    impl->post(2, 10);
    impl->post(2, 11);

    std::vector<int> feed = impl->getNewsFeed(1, 1);

    assert(feed.size() == 1);
    assert(feed.at(0) == 10);
}

void multipleFollowerForOneUser()
{
    Implementation *impl = new Implementation();
    impl->registerUser(1);
    impl->registerUser(2);
    impl->registerUser(3);

    impl->follow(2, 1);
    impl->follow(3, 1);

    impl->post(1, 10);

    std::vector<int> feedForTwo = impl->getNewsFeed(2);

    assert(feedForTwo.size() == 1);
    assert(feedForTwo.at(0) == 10);

    std::vector<int> feedForThree = impl->getNewsFeed(3);

    assert(feedForThree.size() == 1);
    assert(feedForThree.at(0) == 10);
}

void gettingFeedForNonPresentUser()
{
    Implementation *impl = new Implementation();
    try
    {
        std::vector<int> feedForTwo = impl->getNewsFeed(1);
        assert(false);
    }
    catch (std::string &msg)
    {
        assert(msg.compare("UserNotRegisteredException") == 0);
    }
}

void followerNotRegisteredForFollow()
{
    Implementation *impl = new Implementation();
    try
    {
        impl->follow(1, 2);
        assert(false);
    }
    catch (std::string &msg)
    {
        assert(msg.compare("UserNotRegisteredException") == 0);
    }
}

void followerNotRegisteredForUnfollow()
{
    Implementation *impl = new Implementation();
    try
    {
        impl->unfollow(1, 2);
        assert(false);
    }
    catch (std::string &msg)
    {
        assert(msg.compare("UserNotRegisteredException") == 0);
    }
}

void followerNotRegisteredForPost()
{
    Implementation *impl = new Implementation();
    try
    {
        impl->post(1, 10);
        assert(false);
    }
    catch (std::string &msg)
    {
        assert(msg.compare("UserNotRegisteredException") == 0);
    }
}

void followerCanSeeOwnAndOtherUserPosts()
{
    Implementation *impl = new Implementation();

    impl->registerUser(1);
    impl->registerUser(2);
    impl->follow(1, 2);
    impl->post(1, 10);
    impl->post(2, 11);

    std::vector<int> feed = impl->getNewsFeed(1);

    assert(feed.at(0) == 10);
    assert(feed.at(1) == 11);
}

int main()
{
    getFeedForUserOnlyOneTest();
    getFeedForUserOnlyMultiplePostTest();
    followThenAddPostsThenUnfollowThenNoNewsOnFeedTest();
    followAfterPostsButStillSeePosts();
    multiplePostForOneFollowerTest();
    willNotSeePostIfNotFollowingUser();
    willGetFeedWithSize1WithMultiplePosts();
    multipleFollowerForOneUser();
    gettingFeedForNonPresentUser();
    followerNotRegisteredForFollow();
    followerNotRegisteredForUnfollow();
    followerNotRegisteredForPost();
    followerCanSeeOwnAndOtherUserPosts();
    return 0;
}
