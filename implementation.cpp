#include "implementation.h"

const std::string USER_NOT_REGISTERED_ESXCEPTION = std::string("UserNotRegisteredException");
const int DEFAULT_PAGING_SIZE = 5;

Implementation::Implementation()
{
    this->posts = std::vector<std::pair<int, int>>();
    this->follows = std::map<int, std::set<int>>();
}

Implementation::~Implementation()
{
}

void Implementation::registerUser(int userId)
{
    this->mtx.lock();
    this->follows.insert(std::pair<int, std::set<int>>(userId, std::set<int>()));
    this->insertFollowerInFollowees(userId, userId);
    this->mtx.unlock();
}

void Implementation::post(int userId, int postId)
{
    this->mtx.lock();
    if (!isUserRegistered(userId))
    {
        this->mtx.unlock();
        throw USER_NOT_REGISTERED_ESXCEPTION;
    }
    this->posts.push_back(std::pair<int, int>(userId, postId));
    this->mtx.unlock();
}

std::vector<int> Implementation::getNewsFeed(int userId)
{
    return this->getNewsFeed(userId, DEFAULT_PAGING_SIZE);
}

std::vector<int> Implementation::getNewsFeed(int userId, int n)
{
    this->mtx.lock();
    if (!isUserRegistered(userId))
    {
        this->mtx.unlock();
        throw USER_NOT_REGISTERED_ESXCEPTION;
    }
    int count = 0;
    int i = 0;
    int size = this->posts.size();
    std::set<int> followsForUserId = this->follows.at(userId);
    std::vector<int> result = std::vector<int>();
    while (count < n && i < size)
    {
        std::pair<int, int> post = this->posts.at(i);

        if (followsForUserId.count(post.first) > 0)
        {
            result.push_back(post.second);
            ++count;
        }
        ++i;
    }
    this->mtx.unlock();
    return result;
}

void Implementation::follow(int followerId, int followeeId)
{
    this->mtx.lock();
    if (!isUserRegistered(followerId) || !isUserRegistered(followeeId))
    {
        this->mtx.unlock();
        throw USER_NOT_REGISTERED_ESXCEPTION;
    }
    this->insertFollowerInFollowees(followerId, followeeId);
    this->mtx.unlock();
}

void Implementation::unfollow(int followerId, int followeeId)
{
    this->mtx.lock();
    if (!isUserRegistered(followerId) || !isUserRegistered(followeeId))
    {
        this->mtx.unlock();
        throw USER_NOT_REGISTERED_ESXCEPTION;
    }
    this->follows.at(followerId).erase(followeeId);
    this->mtx.unlock();
}

bool Implementation::isUserRegistered(int userId)
{
    return this->follows.find(userId) != this->follows.end();
}

void Implementation::insertFollowerInFollowees(int followerId, int followeeId)
{
    this->follows.at(followerId).insert(followeeId);
}
