#pragma once_too

#include <iostream>
#include <map>
#include <set>
#include <mutex>
#include "interface.h"

class Implementation : public Interface::Interface
{
public:
    /**
     * Constructor.
     */
    Implementation();

    /**
     * Destructor.
     */
    ~Implementation();

    /**
     * Registers a new user.
     */
    void registerUser(int userId);

    /**
     * User adds a new post.
     */
    void post(int userId, int postId);

    /**
     * Retrieve the 5 most recent posts in the user's news feed.
     * Each item in the news feed must be posted
     * by users who the user followed or by the user herself.
     * Posts must be ordered from most recent to least recent.
     */
    std::vector<int> getNewsFeed(int userId);

    /**
     * Retrieve the n most recent posts in the user's news feed.
     * Each item in the news feed must be posted
     * by users who the user followed or by the user herself.
     * Posts must be ordered from most recent to least recent.
     */
    std::vector<int> getNewsFeed(int userId, int n);

    /**
     * Follower follows a followee.
     */
    void follow(int followerId, int followeeId);

    /**
     * Follower unfollows a followee.
     */
    void unfollow(int followerId, int followeeId);

private:
    bool isUserRegistered(int userId);
    void insertFollowerInFollowees(int followerId, int followeeId);
    std::vector<std::pair<int, int>> posts;
    std::map<int, std::set<int>> follows;
    std::mutex mtx;
};